import { Selector } from 'testcafe'
import { register } from './helpers/e2e'

const users = require('./mocks/user.json')

fixture`Test registration form`
  .page`http://localhost:8089/#/register`

users.forEach((user) => {
  test(`Register for user ${user.name}  `, async (browser) => {
    await register({
      name: user.name,
      email: user.email,
      password: user.password,
      browser
    })
    await browser.expect(Selector('#newLoan').exists).ok()
  })
})
