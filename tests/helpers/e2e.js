const register = async ({
                          email,
                          password,
                          name,
                          browser
                        }) => {
  await browser.typeText('#name', name)
  await browser.typeText('#email', email)
  await browser.typeText('#password', password)
  await browser.click('#submit')
}

const applyLoan = async ({
                           amount,
                           terms,
                           browser
                         }) => {
  await browser.typeText('#amount', amount)
  await browser.typeText('#terms', terms)
  await browser.click('#submit')
}


export { register, applyLoan }
