import { createApp } from 'vue'
import App from './App'
import store from './store/index'
import router from './router'
import { i18n } from "./i18n"
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './assets/scss/app.scss'

const {worker} = require('./mocks/browser')
worker.start()

const app = createApp(App)
app.use(router)
app.use(store)
app.use(i18n)
app.use(ElementPlus)
app.mount('#app')
