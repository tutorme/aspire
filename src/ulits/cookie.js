export const Cookie = {
  Create: function (name, value, days, domain) {
    var expires = ''
    let cookieDomain = ''
    if (days) {
      var date = new Date()
      date.setDate(date.getDate() + days)
      expires = '; expires=' + date.toGMTString()
    }
    if (domain) {
      cookieDomain = '; domain=' + domain
    }
    document.cookie = name + '=' + encodeURIComponent(value) + expires + cookieDomain + '; path=/'
  },

  Read: function (name) {
    var nameEQ = name + '='
    var ca = document.cookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) === ' ') c = c.substring(1, c.length)
      if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length))
    }
    return null
  },

  Erase: function (name, domain = null) {
    Cookie.Create(name, '', -1, domain)
  }
}
