import axios from 'axios'

export const mockApi = axios.create({
  baseURL: '',
  headers: {
    'Content-Type': 'application/json'
  }
})
