export const validateFieldsMixin = {
  data() {
    return {
      errors: [],
      allowSave: false
    }
  },
  methods: {
    doValidate(form, conditionObject, field) {
      return field
        ? conditionObject.validateAt(field, form)
          .then(() => {
            this.errors[field] = "";
          })
          .catch(err => {
            this.allowSave = false
            this.errors[err.path] = err.message;
          })
        : conditionObject.validate(form, {abortEarly: false})
          .then(() => {
            this.allowSave = true
          })
          .catch(err => {
            err.inner.forEach(error => {
              this.errors = {...this.errors, [error.path]: error.message};
            })
          })
    }
  }
}
