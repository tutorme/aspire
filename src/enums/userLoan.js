const loanStatus = Object.freeze({
  REPAID: 'REPAID',
  UNPAID: 'UNPAID',
})

const repaymentStatus = Object.freeze({
  REPAID: 'REPAID',
  UNPAID: 'UNPAID',
})

export { loanStatus, repaymentStatus }
