const transactionReasons = Object.freeze({
  STORE: 'store',
  FLIGHT: 'flight',
  SPEAKER: 'speaker'
})

const transactionTypes = Object.freeze({
  REFUND: 'refund',
  CHARGED: 'charged',
})

export { transactionReasons, transactionTypes }
