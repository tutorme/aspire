export const breakpoints = Object.freeze({
    MOBILE_BREAKPOINT: 991,
    TABLET_BREAKPOINT: 1399
})
