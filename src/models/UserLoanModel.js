export class UserLoanModel {
  constructor(loan) {
    this.id = loan.id
    this.userId = loan.userId
    this.amount = Number(loan.amount).format()
    this.terms = loan.terms
    this.status = loan.status
  }
}

