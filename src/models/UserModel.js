export class UserModel {
  constructor(user) {
    this.id = user.id
    this.email = user.email
    this.password = user.password
    this.name = user.name
  }
}
