export class UserRepaymentModel {
  constructor(repayment) {
    this.id = repayment.id
    this.loanId = repayment.loanId
    this.userId = repayment.userId
    this.amount = Number(repayment.amount).format()
    this.datePay = repayment.datePay
    this.status = repayment.status
  }
}

