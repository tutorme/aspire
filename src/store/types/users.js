// Getters
export const IS_USER_REGISTERED = 'users/IS_USER_REGISTERED'
export const USER_PROFILE = 'users/USER_PROFILE'
export const USER_USER_REPAYMENTS = 'users/USER_USER_REPAYMENTS'
export const USER_LOANS = 'users/USER_LOANS'

// Mutations
export const MUTATE_USER_REGISTER = 'users/MUTATE_USER_REGISTER'
export const MUTATE_LOAD_USER_LOANS = 'users/MUTATE_LOAD_USER_LOANS'
export const MUTATE_USER_REPAYMENTS = 'users/MUTATE_USER_REPAYMENTS'
export const MUTATE_USER_REPAY = 'users/MUTATE_USER_REPAY'

// Actions
export const USER_REGISTER = 'users/USER_REGISTER'
export const CREATE_USER_LOAN = 'users/CREATE_USER_LOAN'
export const LOAD_USER_LOANS = 'users/LOAD_USER_LOANS'
export const LOAD_USER_REPAYMENTS = 'users/LOAD_USER_REPAYMENTS'
export const USER_REPAY = 'users/USER_REPAY'
