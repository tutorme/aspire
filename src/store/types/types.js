import * as card from './card'
import * as global from './global'
import * as users from './users'

export const types = Object.assign(
  {},
  card,
  global,
  users
)
