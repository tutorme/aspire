// Getters
export const IS_MOBILE = 'global/IS_MOBILE'
export const IS_TABLET = 'global/IS_TABLET'
export const GET_MENUS = 'global/GET_MENUS'
export const GET_ERROR_TEXT = 'global/GET_ERROR_TEXT'
export const GET_SUCCESS_TEXT = 'global/GET_SUCCESS_TEXT'

export const MUTATE_IS_MOBILE = 'global/MUTATE_IS_MOBILE'
export const MUTATE_IS_TABLET = 'global/MUTATE_IS_TABLET'
export const MUTATE_ERROR_TEXT = 'global/MUTATE_ERROR_TEXT'
export const MUTATE_SUCCESS_TEXT = 'global/MUTATE_SUCCESS_TEXT'
