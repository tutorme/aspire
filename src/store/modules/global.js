import * as global from '../types/global'
import { breakpoints } from '../../enums/breakpoints'
import { userRoutes, userLoan } from '../../router/userRoutes'

const state = {
  isMobileDevice: typeof window.orientation !== 'undefined',
  isMobile: window.innerWidth <= breakpoints.MOBILE_BREAKPOINT,
  errorText: null,
  successText: null,
  menus: [
    {
      id: 1,
      label: 'menu.home.label',
      icon: 'icon-Aspire-Icon',
      routerPath: userRoutes.USER_HOME.path,
      permissions: []
    },
    {
      id: 2,
      label: 'menu.cards.label',
      icon: 'icon-Cards',
      routerPath: userRoutes.USER_CARDS.path,
      permissions: []
    },
    {
      id: 3,
      label: 'menu.payments.label',
      icon: 'icon-Payment',
      routerPath: userRoutes.USER_PAYMENTS.path,
      permissions: []
    },
    {
      id: 4,
      label: 'menu.credit.label',
      icon: 'icon-Up',
      routerPath: userRoutes.USER_CREDIT.path,
      permissions: []
    },
    {
      id: 5,
      label: 'menu.settings.label',
      icon: 'icon-Account',
      routerPath: userLoan.USER_REGISTER.path,
      permissions: []
    }
  ]
}
const getters = {
  [global.IS_MOBILE]: (state) => {
    return state.isMobile
  },
  [global.IS_TABLET]: (state) => {
    return state.isTablet
  },
  [global.GET_MENUS]: (state) => {
    return state.menus
  },
  [global.GET_SUCCESS_TEXT]: (state) => {
    return state.successText
  },
  [global.GET_ERROR_TEXT]: (state) => {
    return state.errorText
  }
}
const mutations = {
  [global.MUTATE_IS_MOBILE]: (state, payload) => {
    state.isMobile = window.innerWidth <= breakpoints.MOBILE_BREAKPOINT;
  },
  [global.MUTATE_IS_TABLET]: (state, payload) => {
    state.isTablet = window.innerWidth <= breakpoints.TABLET_BREAKPOINT && window.innerWidth > breakpoints.MOBILE_BREAKPOINT;
  },
  [global.MUTATE_ERROR_TEXT]: (state, payload) => {
    state.errorText = payload
  },
  [global.MUTATE_SUCCESS_TEXT]: (state, payload) => {
    state.successText = payload
  }
}

export default {
  state,
  getters,
  mutations
}
