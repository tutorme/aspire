import * as card from '../types/card'
import { transactionReasons, transactionTypes } from "../../enums/transaction";

const state = {
  userCards: [
    {
      id: 1,
      cardHolder: 'Mark Henry',
      expiration: '11/20',
      cvv: '598',
      cardNumber: '1111 2222 3333 2020',
      type: 'visa'
    },
    {
      id: 2,
      cardHolder: 'Mike Tyson',
      expiration: '12/25',
      cvv: '690',
      cardNumber: '9999 8888 7777 6666',
      type: 'visa'
    },
    {
      id: 3,
      cardHolder: 'Elon Musk',
      expiration: '09/26',
      cvv: '343',
      cardNumber: '8888 2222 9999 0000',
      type: 'visa'
    }
  ],

  cardTransactions: [
    {
      type: transactionTypes.REFUND,
      reason: transactionReasons.STORE,
      name: 'Hamleys',
      date: '20 May 2021',
      amount: 150,
      currency: 'S$'
    },
    {
      type: transactionTypes.CHARGED,
      reason: transactionReasons.FLIGHT,
      name: 'Hamleys',
      date: '20 May 2021',
      amount: 150,
      currency: 'S$'
    },
    {
      type: transactionTypes.CHARGED,
      reason: transactionReasons.SPEAKER,
      name: 'Hamleys',
      date: '20 May 2021',
      amount: 150,
      currency: 'S$'
    },
    {
      type: transactionTypes.REFUND,
      reason: transactionReasons.STORE,
      name: 'Hamleys',
      date: '20 May 2021',
      amount: 150,
      currency: 'S$'
    }
  ]
}
const getters = {
  [card.GET_USER_CARDS]: (state) => {
    return state.userCards
  },
  [card.GET_CARD_TRANSACTIONS]: (state) => {
    return state.cardTransactions
  }
}
const mutations = {}

export default {
  state,
  getters,
  mutations
}
