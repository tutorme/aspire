import * as users from '../types/users'
import { mockApi } from '../../ulits/api'
import { UserModel } from '../../models/UserModel'
import * as global from "../types/global";
import { Cookie } from "../../ulits/cookie";
import { UserLoanModel } from "../../models/UserLoanModel";
import { loanStatus, repaymentStatus } from "../../enums/userLoan";
import { DateTime } from 'luxon'
import UserRepaymentIndex from "../../components/users/UserRepaymentIndex";
import { UserRepaymentModel } from "../../models/UserRepaymentModel";
import { MUTATE_USER_REPAY } from "../types/users";
import { userLoan } from "../../router/userRoutes";

const state = {
  userProfile: null,
  userLoans: [],
  userRepayments: [],
}

const getters = {
  [users.IS_USER_REGISTERED]: (state) => {
    return state.userProfile !== null
  },
  [users.USER_PROFILE]: (state) => {
    return state.userProfile
  },

  [users.USER_LOANS]: (state) => {
    return state.userLoans
  },

  [users.USER_USER_REPAYMENTS]: (state) => {
    return state.userRepayments
  }
}

const mutations = {
  [users.MUTATE_USER_REGISTER]: (state, user) => {
    Cookie.Create('userId', user.id)
    state.userProfile = new UserModel(user)
  },

  [users.MUTATE_LOAD_USER_LOANS]: (state, loan) => {
    state.userLoans = loan
  },

  [users.MUTATE_USER_REPAYMENTS]: (state, repayments) => {
    state.userRepayments = repayments
  },

  [users.MUTATE_USER_REPAY]: (state, payload) => {
  },
}

const actions = {
  [users.USER_REGISTER]: ({commit}, payload) => {
    return mockApi.post('register', payload).then(response => {
      commit(users.MUTATE_USER_REGISTER, response.data)
      return response
    }).catch(() => {
      commit(global.MUTATE_ERROR_TEXT, 'Error')
    })
  },

  [users.CREATE_USER_LOAN]: ({commit}, payload) => {
    return mockApi.post('/userLoan', payload).then(response => {
      return response
    }).catch(() => {
      commit(global.MUTATE_ERROR_TEXT, 'Error')
    })
  },

  [users.LOAD_USER_LOANS]: ({commit}, payload) => {
    return mockApi.get('/userLoan').then(response => {
      commit(users.MUTATE_LOAD_USER_LOANS, response.data)
    }).catch(() => {
      commit(global.MUTATE_ERROR_TEXT, 'Error')
    })
  },

  [users.LOAD_USER_REPAYMENTS]: ({commit}, payload) => {
    return mockApi.get('/userRepayment', {params: payload}).then(response => {
      commit(users.MUTATE_USER_REPAYMENTS, response.data)
    }).catch(() => {
      commit(global.MUTATE_ERROR_TEXT, 'Error')
    })
  },

  [users.USER_REPAY]: ({commit}, payload) => {
    return mockApi.put('/repay', payload).then(response => {
      commit(users.MUTATE_USER_REPAY, payload)
    }).catch(() => {
      commit(global.MUTATE_ERROR_TEXT, 'Error')
    })
  },
}

Number.prototype.format = function (n, x) {
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
  return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

export default {
  state,
  mutations,
  actions,
  getters
}
