import { createStore } from 'vuex'
import global from './modules/global'
import card from './modules/card'
import users from './modules/users'

export default createStore({
  modules: {
    global,
    card,
    users
  }
})
