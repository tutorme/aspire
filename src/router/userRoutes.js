const userRoutes = {
  USER_HOME: {
    name: 'UserHome',
    path: '/',
    meta: {
      title: 'Aspire'
    }
  },
  USER_CARDS: {
    name: 'UserCard',
    path: '/cards',
    meta: {
      title: 'Cards'
    }
  },
  USER_PAYMENTS: {
    name: 'UserPayments',
    path: '/payments',
    meta: {
      title: 'Payments'
    }
  },
  USER_CREDIT: {
    name: 'UserCredit',
    path: '/credit',
    meta: {
      title: 'Credit'
    }
  },
  USER_SETTINGS: {
    name: 'UserSettings',
    path: '/settings',
    meta: {
      title: 'Settings'
    }
  }
}

const userLoan = {
  USER_REGISTER: {
    name: 'UserRegister',
    path: '/register',
    meta: {
      title: 'User Register'
    }
  },
  USER_DASHBOARD: {
    name: 'UserDashboard',
    path: '/user',
    meta: {
      title: 'User Dashboard'
    }
  },
  USER_LOANS: {
    name: 'UserLoans',
    path: 'user-loans',
    meta: {
      title: 'User Loans'
    }
  },
  USER_APPLY_LOAN: {
    name: 'UserApplyLoan',
    path: 'apply',
    meta: {
      title: 'User Apply Loan'
    }
  },

  USER_REPAY_SCHEDULE: {
    name: 'UserRepaySchedule',
    path: 'repay-schedule/:loanId',
    meta: {
      title: 'Repay Schedule'
    }
  },
}

export { userRoutes, userLoan }
