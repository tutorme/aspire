import { createRouter, createWebHistory } from 'vue-router'
import { userRoutes, userLoan } from './userRoutes'
import CardIndex from "../components/cards/CardIndex"
import UserDashboard from "../components/users/UserDashboard";
import UserRegister from "../components/users/UserRegister";

const routes = [
  {
    path: '/',
    name: userRoutes.USER_HOME.name,
    components: {
      default: CardIndex
    },
    meta: userRoutes.USER_HOME.meta
  },
  {
    path: userRoutes.USER_CARDS.path,
    name: userRoutes.USER_CARDS.name,
    components: {
      default: CardIndex
    },
    meta: userRoutes.USER_CARDS.meta
  },
  {
    path: userRoutes.USER_PAYMENTS.path,
    name: userRoutes.USER_PAYMENTS.name,
    components: {
      default: CardIndex
    },
    meta: userRoutes.USER_PAYMENTS.meta
  },
  {
    path: userRoutes.USER_CREDIT.path,
    name: userRoutes.USER_CREDIT.name,
    components: {
      default: CardIndex
    },
    meta: userRoutes.USER_CREDIT.meta
  },
  {
    path: userRoutes.USER_SETTINGS.path,
    name: userRoutes.USER_SETTINGS.name,
    components: {
      default: CardIndex
    },
    meta: userRoutes.USER_SETTINGS.meta
  },
  {
    path: userLoan.USER_REGISTER.path,
    name: userLoan.USER_REGISTER.name,
    components: {
      default: UserRegister
    },
    meta: userLoan.USER_REGISTER.meta,
  },
  {
    path: userLoan.USER_DASHBOARD.path,
    name: userLoan.USER_DASHBOARD.name,
    components: {
      default: UserDashboard
    },
    meta: userLoan.USER_REGISTER.meta,
    children: [
      {
        path: userLoan.USER_LOANS.path,
        name: userLoan.USER_LOANS.name,
        component: () => import('../components/users/UserLoanIndex'),
        meta: userRoutes.USER_SETTINGS.meta,
        children: [
          {
            path: userLoan.USER_APPLY_LOAN.path,
            name: userLoan.USER_APPLY_LOAN.name,
            component: () => import('../components/users/UserLoanForm'),
            meta: userRoutes.USER_SETTINGS.meta
          },
        ]
      },

      {
        path: userLoan.USER_REPAY_SCHEDULE.path,
        name: userLoan.USER_REPAY_SCHEDULE.name,
        component: () => import('../components/users/UserRepaymentIndex'),
        meta: userRoutes.USER_SETTINGS.meta
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory('/#/'),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})

export default router
