import { rest } from 'msw'
import { loanStatus, repaymentStatus } from "../enums/userLoan";
import { DateTime } from "luxon";
import { Cookie } from "../ulits/cookie";

const userId = Cookie.Read('userId')

const uuid = () => {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  )
}

const addUsers = (user) => {
  user.id = uuid()
  let users = JSON.parse(localStorage.getItem("users") || "[]");
  users.push(user)
  localStorage.setItem('users', JSON.stringify(users))
}

const loadUserRepayments = (userId, loanId) => {
  const repayments = JSON.parse(localStorage.getItem("repayments") || "[]");
  return repayments ? repayments.filter(s => s.userId === userId && s.loanId === loanId) : []
}

const updateUserRepayment = (userId, repaymentId) => {
  const repayments = JSON.parse(localStorage.getItem("repayments") || "[]");
  let repayment = repayments.find(s => s.userId === userId && s.id === repaymentId)
  if (repayment) {
    repayment.status = repaymentStatus.REPAID

  }
  const unPaidLeft = repayments.find(r => r.userId === userId && r.status === repaymentStatus.UNPAID)
  localStorage.setItem('repayments', JSON.stringify(repayments))
  if (!unPaidLeft) {
    updateLoan(repayment.loanId)
  }
  return repayment
}

const loadUserLoans = (userId) => {
  const loan = JSON.parse(localStorage.getItem("userLoans") || "[]");
  return loan ? loan.filter(s => s.userId === userId) : []
}

const updateLoan = (id) => {
  let userLoans = JSON.parse(localStorage.getItem("userLoans") || "[]");
  let loanToUpdate = userLoans.find(l => l.id === id)
  if (loanToUpdate) {
    loanToUpdate.status = loanStatus.REPAID
    localStorage.setItem('userLoans', JSON.stringify(userLoans))
  }
}


const addLoan = (loan) => {
  let userLoans = loadUserLoans(loan.userId)
  userLoans.push(loan)
  localStorage.setItem('userLoans', JSON.stringify(userLoans))
  let repayments = JSON.parse(localStorage.getItem("repayments") || "[]");

  const monthlyAmount = Number((loan.amount / loan.terms).toFixed(2))
  for (let i = 0; i < loan.terms; i++) {
    const repayment = {
      id: uuid(),
      loanId: loan.id,
      userId: loan.userId,
      datePay: DateTime.now().plus({months: (i + 1)}).toFormat('yyyy-MM-dd'),
      amount: monthlyAmount,
      status: repaymentStatus.UNPAID
    }
    repayments.push(repayment)
  }
  localStorage.setItem('repayments', JSON.stringify(repayments))
}

export default [
  rest.get('/repaySchedules', (req, res, ctx) => {
    return res(
      ctx.json(req.body)
    )
  }),
  rest.post('/register', (req, res, ctx) => {
    addUsers(req.body)
    return res(
      ctx.json(req.body)
    )
  }),
  rest.get('/register', (req, res, ctx) => {
    return res(
      ctx.json(req.body)
    )
  }),
  rest.get('/repaySchedules', (req, res, ctx) => {
    return res(
      ctx.json(req.body)
    )
  }),
  rest.post('/userLoan', (req, res, ctx) => {
    const userId = Cookie.Read('userId')
    let loan = req.body
    loan.userId = userId
    loan.id = uuid()
    loan.status = loanStatus.UNPAID
    addLoan(loan)
    return res(
      ctx.json(loan)
    )
  }),
  rest.get('/userLoan', (req, res, ctx) => {
    return res(
      ctx.json(loadUserLoans(userId))
    )
  }),
  rest.get('/userRepayment', (req, res, ctx) => {
    return res(
      ctx.json(loadUserRepayments(userId, req.url.searchParams.get('loanId')))
    )
  }),
  rest.put('/repay', (req, res, ctx) => {
    return res(
      ctx.json(updateUserRepayment(userId, req.body.id))
    )
  }),


]
