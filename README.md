# Aspire

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### 1 Code challenge deployment on Netlify at
```
https://eloquent-curran-459976.netlify.app/#/register
```

### 2 CSS challenge deployment on Netlify at
```
https://eloquent-curran-459976.netlify.app/
```

### For automated test. The Testcafe framework is used.
After finished the project setup. Following below commands
```
$ cd <project-folder>/tests/
$ testcafe chrome register.js
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
